var socket = io.connect();

  socket.on('scoreChanged', function (scores) {
    $('.teamA-points').text(scores.teamA.points);
    $('.teamA-sets').text(scores.teamA.sets);
    $('.teamA-name').text(scores.teamA.name);
    $('.teamB-points').text(scores.teamB.points);
    $('.teamB-sets').text(scores.teamB.sets);
    $('.teamB-name').text(scores.teamB.name);
  });

$(function() {
  $('.button .target').fastClick(function() {
    if ($(this).hasClass('teamA')) {
    	socket.emit('score-teamA');
    }
    if ($(this).hasClass('teamB')) {
    	socket.emit('score-teamB');
    }

  });

  $('.scoreboard .left').fastClick(function() {
    socket.emit('score-teamA');
  });

  $('.scoreboard .right').fastClick(function() {
    socket.emit('score-teamB');
  });

  $('.button .reset').fastClick(function() {
  	socket.emit('reset-scores');
  });


});
