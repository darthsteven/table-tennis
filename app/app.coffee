config = require("app/config")
express = require("express")
path = require('path')
handlebars = require('express3-handlebars')
io = require('socket.io')
http = require('http')
scores = require("app/scoreboard")

# Setup scoreboard
scoreboard = new scores.Scoreboard()

app = express()
app.enable "trust proxy"

app.engine 'handlebars', handlebars
  defaultLayout: 'main'

app.set('view engine', 'handlebars')

# See the README in https://github.com/focusaurus/express_code_structure
AUTOUSING_THE_ROUTER_IS_A_NUISANCE = app.router

app.use(express.static(__dirname + '/public'));

# Zero-cache middleware (this will get replaced later)
app.use (req, res, next) ->
  res.header "Cache-Control", "no-cache, no-store, must-revalidate"
  res.header "Pragma", "no-cache"
  res.header "Expires", 'Sun, 17 Aug 1988 05:00:00 GMT'
  next() # http://expressjs.com/guide.html#passing-route control

# Pop the scoreboard into the request.
app.use (req, res, next) ->
  req.scoreboard = scoreboard
  next() # http://expressjs.com/guide.html#passing-route control

# We log all requests
if config.env is "development"
  app.use (req, res, next) ->
    console.log '%s %s', req.method, req.url
    next()

# Load our routes ("controllers" -ish)
require(routePath)(app) for routePath in [
  "app/general/routes"
]

# OK, routes are loaded, NOW use the router:
app.use app.router
app.use express.bodyParser()


# development only
if config.env is 'development'
  app.use express.errorHandler()

# Setup socket.io
server = http.createServer(app)
io = require('socket.io').listen(server)

io.sockets.on 'connection', (socket) ->
  scoreboard.emitScore()


scoreboard.on 'scoreChanged', (score) ->
  io.sockets.emit 'scoreChanged', score

io.sockets.on 'connection', (socket) ->
  socket.on 'score-teamA', (data) ->
    scoreboard.teamAPoint()
  socket.on 'score-teamB', (data) ->
    scoreboard.teamBPoint()
  socket.on 'reset-scores', (data) ->
    console.log data
    scoreboard.resetScores()


server.listen config.express.port

console.log "App listening on port #{config.express.port}"
