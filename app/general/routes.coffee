forms = require('forms')
fields = forms.fields
validators = forms.validators;

ping = (req, res) ->
  res.send "<h1>PONG from node.js: #{Date()}</h1>"

scoreboard = (req, res) ->
  res.render 'scoreboard'

button = (req, res) ->
  res.render 'button'


reg_form = forms.create(
  teamAname: fields.string(
    required: true
    label: 'Team A Name'
  )
  teamBname: fields.string(
    required: true
    label: 'Team B Name'
  )
);

configure = (req, res) ->
  sb = req.scoreboard

  data =
    teamAname: sb.teamAName()
    teamBname: sb.teamBName()

  bound_form = reg_form.bind(data)

  res.render 'configure',
    form: bound_form.toHTML()

configure_post = (req, res) ->
  sb = req.scoreboard

  reg_form.handle req,
    # Handle correctly validated input
    success: (form) ->
      sb.teamAName form.data.teamAname
      sb.teamBName form.data.teamBname
      res.render 'configure',
        message: 'Saved.'
        form: form.toHTML()

    # Handle errors etc.
    other:  (form) ->
      # This totally doesn't really work.
      res.render 'configure',
        form: form.toHTML()


module.exports = (app) ->
  app.get '/nodejs/ping', ping
  app.get '/', scoreboard
  app.get '/button', button
  app.get '/configure', configure
  app.post '/configure', configure_post
