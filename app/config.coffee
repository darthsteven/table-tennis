###
  This file is just a place to stick bits of global configuration, because,
  well, it's nice to have.
###
config = module.exports;

config.express =
  port: process.env.EXPRESS_PORT || 3000

# We default to 'development'
config.env = process.env.NODE_ENV ? "development"
