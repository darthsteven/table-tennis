util = require("util")
events = require("events")

class Scoreboard extends events.EventEmitter
  constructor: () ->
    #setInterval @randomUpdate, 1000
    @score =
      teamA:
        points: 0
        sets: 0
        name: "Team A"
      teamB:
        points: 0
        sets: 0
        name: "Team B"

  randomUpdate: =>
    if Math.random() >= 0.5
      @teamAPoint()
    else
      @teamBPoint()

  teamAPoint: =>
    @score.teamA.points++
    @checkScores()

  teamBPoint: =>
    @score.teamB.points++
    @checkScores()

  teamAUndo: =>
    @score.teamA.points--
    @checkScores()

  teamBUndo: =>
    @score.teamB.points--
    @checkScores()

  resetScores: =>
    @score.teamA.points = 0
    @score.teamA.sets = 0
    @score.teamB.points = 0
    @score.teamB.sets = 0
    @checkScores()

  checkScores: =>
    # Just emit the score, and we'll do sets later.
    if @score.teamA.points >= 11
      @score.teamA.points = 0
      @score.teamB.points = 0
      @score.teamA.sets++
    if @score.teamB.points >= 11
      @score.teamA.points = 0
      @score.teamB.points = 0
      @score.teamB.sets++
    @emitScore()

  emitScore: =>
    @emit 'scoreChanged', @score

  teamAName: (name) =>
    if name?
      @score.teamA.name = name
      @emit 'scoreChanged', @score
    return @score.teamA.name

  teamBName: (name) =>
    if name?
      @score.teamB.name = name
      @emit 'scoreChanged', @score
    return @score.teamB.name



module.exports.Scoreboard = Scoreboard
